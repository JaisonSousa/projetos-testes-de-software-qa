package br.ce.jaison.appium;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import br.ce.jaison.appium.core.BaseTest;
import br.ce.jaison.appium.core.DriverFactory;
import br.ce.jaison.appium.page.FomularioPage;
import br.ce.jaison.appium.page.MenuPage;
import io.appium.java_client.MobileBy;
import io.appium.java_client.functions.ExpectedCondition;


//LIMPA OS IMPORT = CTRL +SHIFT + O
//CONSULTA ITEM NO PROJETO = CTRL +SHIFT + R

public class FormularioTeste extends BaseTest {
	
	//private AndroidDriver<MobileElement> driver;
	private MenuPage menu = new MenuPage();
	private FomularioPage page = new FomularioPage();
	
	@Before
	public void inicializarAppium() throws MalformedURLException {
		//driver = DriverFactory.getDriver();
		
		//Selecionar opção formulario
		menu.acessarFormulario();
		//driver.findElement(By.xpath("//*[@text='Formulário']")).click();
	}
	
	@Test
	public void devePreecherCampoTexto() throws MalformedURLException {
		
		//Selecionar opção formulario
//		List<MobileElement> elementosEncontrados = driver.findElements(By.className("android.widget.TextView"));
//		for (MobileElement elemento : elementosEncontrados) {
//			System.out.println(elemento.getText());
//			
//		}
		
//		elementosEncontrados.get(1).click();
		
		//Escrever nome
		page.escreverNome("Jaison");
//		MobileElement campoNome = driver.findElement(MobileBy.AccessibilityId("nome"));
//		campoNome.sendKeys("Jaison");
		Assert.assertEquals("Jaison", page.obterNome());
		
		//Checar nome escrito
//		String nome = campoNome.getText();
//		Assert.assertEquals("Jaison", campoNome.getText());

	}
	
	@Test
	public void deveInteragirCombo() throws MalformedURLException {
		
		//Selecionar opção formulario
		//driver.findElement(By.xpath("//android.widget.TextView[@text='Formulário']")).click();
		
		//clicar no combo
		//driver.findElement(MobileBy.AccessibilityId("console")).click();
		page.selecionarCombo("Nintendo Switch");
		
		//selecionar opcao desejada
		//driver.findElement(By.xpath("//android.widget.CheckedTextView[@text='Nintendo Switch']")).click();
		
		//verificar apcao selecionada
		String texto = page.obterValorCombo();
		//String texto = driver.findElement(By.xpath("//android.widget.Spinner/android.widget.TextView")).getText();
		Assert.assertEquals("Nintendo Switch", texto);

	}
	
	@Test
	public void deveInteragirSwitchCheckbox() throws MalformedURLException {
		
		//verificar status dos elementos
		Assert.assertFalse(page.isCheckMarcado());
		
		//Assert.assertTrue(check.getAttribute("checked").equals("false"));
		Assert.assertTrue(page.isSmitchMarcado());
		//Assert.assertTrue(switc.getAttribute("checked").equals("true"));
		
		//clicar nos elementos
		//dsl.clicar(By.className("android.widget.CheckBox"));
		page.clicarCheck();
		//dsl.clicar(MobileBy.AccessibilityId("switch"));
		page.clicarSwitch();
		
//		MobileElement check = driver.findElement(By.className("android.widget.CheckBox"));
//		MobileElement switc = driver.findElement(MobileBy.AccessibilityId("switch"));
//		check.click();
//		switc.click();
		
		//verificar estados alterados
		Assert.assertTrue(page.isCheckMarcado());
		Assert.assertFalse(page.isSmitchMarcado());
		
//		Assert.assertFalse(check.getAttribute("checked").equals("false"));
//		Assert.assertFalse(switc.getAttribute("checked").equals("true"));

	}
	
	@Test
	public void deveRealizarCadastro() throws MalformedURLException {
	
		//Selecionar opção formulario
	    //driver.findElement(By.xpath("//*[@text='Formulário']")).click();
	    
	    //preencher campos
	    //driver.findElement(By.className("android.widget.EditText")).sendKeys("Jaison");
		//dsl.escrever(By.className("android.widget.EditText"), "Jaison");
		page.escreverNome("Jaison");
	    //driver.findElement(By.className("android.widget.CheckBox")).click();
		//dsl.clicar(By.className("android.widget.CheckBox"));
		page.clicarCheck();
//	    driver.findElement(By.className("android.widget.Switch")).click();
		//dsl.clicar(By.className("android.widget.Switch"));
		page.clicarSwitch();
//	    driver.findElement(By.className("android.widget.Spinner")).click();
//		dsl.clicar(By.className("android.widget.Spinner"));
		//dsl.selecionarCombo(By.className("android.widget.Spinner"), "Nintendo Switch");
		page.selecionarCombo("Nintendo Switch");
	    //driver.findElement(By.xpath("//android.widget.CheckedTextView[@text='Nintendo Switch']")).click();
		
	    //salvar
		page.salvar();
	    //driver.findElement(By.xpath("//*[@text='SALVAR']")).click();
	    
	    //verificações
	    //String nome = dsl.obterTexto(By.xpath("//android.widget.TextView[@text='Nome: Jaison']"));
	    Assert.assertEquals("Nome: Jaison", page.obterNomeCadastrado());
	    
	    //String combo = dsl.obterTexto(By.xpath("//android.widget.TextView[starts-with(@text, 'Console:')]"));
	    Assert.assertEquals("Console: switch", page.obterConsoleCadastrado());
	    
	    //String swit = dsl.obterTexto(By.xpath("//android.widget.TextView[starts-with(@text, 'Switch:')]"));
	    Assert.assertTrue(page.obterSwitchCadastrado().endsWith("Off"));
	    
	    //String Check = dsl.obterTexto(By.xpath("//android.widget.TextView[starts-with(@text, 'Checkbox:')]"));
	    Assert.assertTrue(page.obterCheckCadastrado().endsWith("Marcado"));
	    	
	}
	
	@Test
	public void deveRealizarCadastroDemorado() throws MalformedURLException {
		
	
		page.escreverNome("Jaison");

		DriverFactory.getDriver().manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
		page.salvarDemorado();
		//esperar(3000);
		
		// Outra forma de espera explicita
		WebDriverWait wait = new WebDriverWait(DriverFactory.getDriver(), 10);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@text='Nome: Jaison']")));
	  
	    Assert.assertEquals("Nome: Jaison", page.obterNomeCadastrado());
	        	
	}
	
	@Test
	public void devoAlterarData()
	{
		page.clicarPorTexto("01/01/2000");
		page.clicarPorTexto("20");
		page.clicarPorTexto("OK");
		Assert.assertTrue(page.existeElementoPorTexto("20/2/2000"));
	}
	
	@Test
	public void devoAlterarHora()
	{
		page.clicarPorTexto("12:00");
		page.clicar(MobileBy.AccessibilityId("10"));
		page.clicar(MobileBy.AccessibilityId("40"));
		page.clicarPorTexto("OK");
		Assert.assertTrue(page.existeElementoPorTexto("10:40"));
	}
	
	@Test
	public void devoInteragirComSeekbar()
	{
		//clicar no seekbar
		page.clicarSeekBar(0.67);
		//salvar
		page.salvar();
		//obter o valor
	}

}
