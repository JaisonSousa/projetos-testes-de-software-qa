package br.ce.jaison.appium.test;

import org.junit.Assert;
import org.junit.Test;

import br.ce.jaison.appium.core.BaseTest;
import br.ce.jaison.appium.page.MenuPage;
import br.ce.jaison.appium.page.SwipeListPage;

public class SwipeElementTeste extends BaseTest {
	
	private MenuPage menu = new MenuPage();
	private SwipeListPage page = new SwipeListPage();
	
	
	@Test
	public void deveResolverDesafio() {
		// clicar swipe list
		menu.clicarSwipeList();
		
		//op1 para direita
		page.swipeElementRight("Opção 1");
		
		//ap1 +
		page.clicarBotaoMais();
		
		//verificar op1+
		Assert.assertTrue(page.existeElementoPorTexto("Opção 1 (+)"));
		
		//op4 para direita
		page.swipeElementRight("Opção 4");
		
		//op4-
		page.clicarPorTexto("(-)");
		
		//verificar op4-
		Assert.assertTrue(page.existeElementoPorTexto("Opção 4 (-)"));
		
		//op5 para esquerda
		page.swipeElementLeft("Opção 5 (-)");
		
		//verificar op5
		Assert.assertTrue(page.existeElementoPorTexto("Opção 5"));

	}

}
