package br.ce.jaison.appium.test;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import br.ce.jaison.appium.core.BaseTest;
import br.ce.jaison.appium.page.MenuPage;
import br.ce.jaison.appium.page.SplashPage;

public class SplashTeste extends BaseTest {
	
	private MenuPage menu = new MenuPage();
	private SplashPage splash = new SplashPage();
	
	@Test
	public void devoAguardarSplashSumir() {
		//acessar menu splash
		menu.acessarSplash();
		//verificar que o splash esta sendo exibido
		splash.isTelaSplashPorTexto();
		//aguardar saida do splash
		splash.aguardarSplashSumir();
		//verificar que formulario está aparecendo
		assertTrue(splash.existeElementoPorTexto("Formulário"));

	}

}
