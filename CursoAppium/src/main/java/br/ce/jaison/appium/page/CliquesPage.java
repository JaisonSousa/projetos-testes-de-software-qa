package br.ce.jaison.appium.page;

import static br.ce.jaison.appium.core.DriverFactory.getDriver;

import javax.swing.text.Element;

import org.openqa.selenium.By;

import br.ce.jaison.appium.core.BasePage;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidTouchAction;
import io.appium.java_client.touch.offset.ElementOption;

public class CliquesPage extends BasePage {
	
	public void cliqueLogo() {
		
		cliqueLogo(By.xpath("//*[@text='Clique Longo']"));
	}
	
	public String obterTextoCampo() {
		
		return getDriver().findElement(By.xpath("(//android.widget.TextView)[3]")).getText();
		
	}

}