package br.ce.jaison.appium.test;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import br.ce.jaison.appium.core.BaseTest;
import br.ce.jaison.appium.page.CliquesPage;
import br.ce.jaison.appium.page.MenuPage;

public class CliquesTeste extends BaseTest {
	
	private MenuPage menu = new MenuPage();
	private CliquesPage page = new CliquesPage();
	
	@Before
	public void setup() {
		//acessar menu
		menu.acessarCliques();

	}
	
	@Test
	public void deveRealizarCliqueLongo() {
			
		//clique Logo
		page.cliqueLogo();
		
		//verificar texto
		Assert.assertEquals("Clique Longo", page.obterTextoCampo());
		
	}
	
	@Test
	public void deveRealizarCliqueDuplo() {
		
		page.clicarPorTexto("Clique duplo");
		page.clicarPorTexto("Clique duplo");
		
		esperar(4000);
		//verificar texto
		Assert.assertEquals("Duplo Clique", page.obterTextoCampo());
		
	}
	

}
