package br.ce.jaison.appium.core;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.junit.Before;
import org.openqa.selenium.By;
import org.openqa.selenium.MutableCapabilities;
import org.openqa.selenium.remote.DesiredCapabilities;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;

public class DriverFactory {
	
    private static AndroidDriver<MobileElement> driver;
    
    public static AndroidDriver<MobileElement> getDriver() {
    	if (driver == null) {
			//createDriver();
    		createDriversaucelabs();
		}
    	
    	return driver;
		
	}
	
	@Before
	private static void createDriver() {
		
		DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
		desiredCapabilities.setCapability("platformName", "Android");
//		//emulado web
//		desiredCapabilities.setCapability("oauth-jaison.sp13-f1539", "3dcb4e45-9e8e-4dcb-9962-0c199b7055cd");
		desiredCapabilities.setCapability("deviceName", "Emulator-5554");
		desiredCapabilities.setCapability("automationName", "uiautomator2");
//		desiredCapabilities.setCapability("noReset", true);
		
//		desiredCapabilities.setCapability("appPackage", "com.google.android.calculator");
//		desiredCapabilities.setCapability("appActivity", "com.android.calculator2.Calculator");
		desiredCapabilities.setCapability(MobileCapabilityType.APP, "C:\\Users\\Consultor\\Desktop\\CTAppium_2_0.apk");
		
//		desiredCapabilities.setCapability("appWaitPackage", "com.google.android.permissioncontroller");
//		desiredCapabilities.setCapability("appWaitActivity", "com.android.packageinstaller.permission.ui.ReviewPermissionsActivity");
		
		try {
			driver = new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"), desiredCapabilities);
			//driver = new AndroidDriver<MobileElement>(new URL("https://oauth-jaison.sp13-f1539:3dcb4e45-9e8e-4dcb-9962-0c199b7055cd@ondemand.us-west-1.saucelabs.com:443/wd/hub"), desiredCapabilities);

		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
//		driver.findElement(By.xpath("//android.widget.Button[@text='Continue']")).click();
//		driver.findElement(By.xpath("//*[@text='OK']")).click();
	}
	
	private static void createDriversaucelabs() {
		
		MutableCapabilities caps = new MutableCapabilities();
		caps.setCapability("platformName", "Android");
    	caps.setCapability("deviceName","Samsung Galaxy S9 Plus FHD GoogleAPI Emulator");
    	caps.setCapability("platformVersion","8.1");
		//caps.setCapability("appium:platformVersion", "11");
		//caps.setCapability("appium:deviceName", "Samsung.*Galaxy.*");
		//caps.setCapability("appium:orientation", "portrait");
//		caps.setCapability("automationName", "uiautomator2");
		caps.setCapability("appium:app", "storage:filename=CTAppium_2_0.apk");
		MutableCapabilities sauceOptions = new MutableCapabilities();
		sauceOptions.setCapability("username", "oauth-jaison.sp13-f1539");
		sauceOptions.setCapability("accessKey", "3dcb4e45-9e8e-4dcb-9962-0c199b7055cd");
		caps.setCapability("sauce:options", sauceOptions);
		
		try {
			
			driver = new AndroidDriver<MobileElement>(new URL("https://oauth-jaison.sp13-f1539:3dcb4e45-9e8e-4dcb-9962-0c199b7055cd@ondemand.us-west-1.saucelabs.com:443/wd/hub"), caps);

		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
	}
	
	
	public static void killDriver() {
		if (driver != null) {
			driver.quit();
			driver = null;
		}
		
	}

}
