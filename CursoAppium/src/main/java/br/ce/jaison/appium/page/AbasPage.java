package br.ce.jaison.appium.page;

import br.ce.jaison.appium.core.BasePage;

public class AbasPage extends BasePage {
	
	public boolean isAba1() {
		
		return existeElementoPorTexto("Este é o conteúdo da Aba 1");	
	}
	
    public boolean isAba2() {
		
    	return existeElementoPorTexto("Este é o conteúdo da Aba 2");	
	}
    
   public void selecionarAba2() {
		
		clicarPorTexto("Aba 2");	
	}

}
