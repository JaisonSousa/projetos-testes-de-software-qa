package br.ce.jaison.appium.test;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import br.ce.jaison.appium.core.BaseTest;
import br.ce.jaison.appium.page.AlertaPage;
import br.ce.jaison.appium.page.MenuPage;

public class AlertTeste extends BaseTest {
	
	private MenuPage menu = new MenuPage();
	private AlertaPage page = new AlertaPage();
	
	@Before
	public void setup() {
		menu.acessarAlerta();
	}
	
	@Test
	public void deveConfirmarAlerta() {
		
		//acessar menu alerta
		menu.acessarAlerta();
		
		//clicar em alert confirm
		page.clicaAlertaConfirm();
		
		//verificaros os textos
		Assert.assertEquals("Info", page.obterTituloAlerta());
		Assert.assertEquals("Confirma a operação?", page.obterMensagemAlerta());
		
		//confirmar alerta
		page.confimar();
		
		//verificar nova mensagem
		Assert.assertEquals("Confirmado", page.obterMensagemAlerta());
		
		//sair
		page.sair();
		
	}
	
	@Test
	public void deveClicarForaAlerta() {
		
		//clicar alerta simples
		page.clicaAlertaSimples();
		esperar(1000);
		//clicar fora da caixa
		page.clicaForaCaixa();
		//verificar que a mensagem está presente
		Assert.assertFalse(page.existeElementoPorTexto("Pode clicar no OK ou fora da caixa para sair"));
	}
	

}
