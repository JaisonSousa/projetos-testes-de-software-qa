package br.ce.jaison.appium.test;

import org.junit.Assert;
import org.junit.Test;

import br.ce.jaison.appium.core.BaseTest;
import br.ce.jaison.appium.page.MenuPage;

public class SwipeTeste extends BaseTest {
	
	private MenuPage menu = new MenuPage();
	
	@Test
	public void deveRealizarSwipe() {
		
		//acessar menu
		menu.acessarSwipe();
		//verificar texto 'a esqueda'
		Assert.assertTrue(menu.existeElementoPorTexto("a esquerda"));
		
		//swipe para direita
		menu.swipeRight();
		//verificar texto 'E veja se'
		Assert.assertTrue(menu.existeElementoPorTexto("E veja se"));
		//clicar no botão direito
		menu.clicarPorTexto("›");
		//verificar texto 'Chegar até o fim'
		Assert.assertTrue(menu.existeElementoPorTexto("Chegar até o fim!"));
		//swipe para esquerda
		menu.swipeLeft();
		//clicar no botão esquendo
		menu.clicarPorTexto("‹");
		//verificar texto 'a esquerda'
		Assert.assertTrue(menu.existeElementoPorTexto("a esquerda"));
		
	}

}
