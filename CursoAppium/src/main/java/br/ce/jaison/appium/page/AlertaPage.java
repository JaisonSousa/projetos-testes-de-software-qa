package br.ce.jaison.appium.page;

import org.openqa.selenium.By;

import br.ce.jaison.appium.core.BasePage;

public class AlertaPage extends BasePage {
	
	public void clicaAlertaConfirm() {
		
		clicarPorTexto("ALERTA CONFIRM");
		
	}
	
  public void clicaAlertaSimples() {
		
		clicarPorTexto("ALERTA SIMPLES");
		
	}
	
   public void confimar() {
		
		clicarPorTexto("CONFIRMAR");
		
	}
   
   public void sair() {
		
		clicarPorTexto("SAIR");
		
	}
   
   public void clicaForaCaixa() {
		
		tap(100, 150);
		
	}

}
