package br.ce.jaison.appium.page;

import static br.ce.jaison.appium.core.DriverFactory.getDriver;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import br.ce.jaison.appium.core.BasePage;

public class MenuPage extends BasePage {
	
	public void acessarFormulario() {
		clicarPorTexto("Formulário");		
	}
	
	public void acessarSplash() {
		clicarPorTexto("Splash");		
	}
	
	public void acessarAlerta() {
		clicarPorTexto("Alertas");		
	}
	
	public void acessarAbas() {
		clicarPorTexto("Abas");		
	}
	
	public void acessarAccordion() {
		clicarPorTexto("Accordion");		
	}
	
	public void acessarCliques() {
		clicarPorTexto("Cliques");		
	}
	
	public void acessarSwipe() {
		clicarPorTexto("Swipe");		
	}
	
	public void acessarSBHibrido() {
		clicarPorTexto("SeuBarriga Híbrido");		
	}
	
	public void acessarSBNativo() {
		clicarPorTexto("SeuBarriga Nativo");		
	}
	
	public void clicarSwipeList() {
		
		WebDriverWait wait = new WebDriverWait(getDriver(), 10);
		wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//*[@text='Formulário']")));
		
		//scrool down
		scrollDown();
		//clicar menu
		clicarPorTexto("Swipe List");
		
	}
	
    public void clicarDragNDrop() {
		
		WebDriverWait wait = new WebDriverWait(getDriver(), 10);
		wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//*[@text='Formulário']")));
		
		//scrool down
		scrollDown();
		//clicar menu
		clicarPorTexto("Drag and drop");
		
	}

}
