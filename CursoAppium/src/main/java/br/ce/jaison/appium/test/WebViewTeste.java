package br.ce.jaison.appium.test;

import org.junit.Assert;
import org.junit.Test;

import br.ce.jaison.appium.core.BaseTest;
import br.ce.jaison.appium.page.MenuPage;
import br.ce.jaison.appium.page.WebViewPage;


public class WebViewTeste extends BaseTest {
	
	private MenuPage menu = new MenuPage();
	private WebViewPage page = new WebViewPage();
	
	@Test
	public void deveFazerLogin() {
		
		//acessar menu
		menu.acessarSBHibrido();
		
		esperar(3000);
		page.entrarContextoWeb();
		//preencher email
		page.setEmail("a@a");
		//senha
		page.setSenha("123456");
		
		//entrar
		page.entrar();
		
		//verificar
		Assert.assertEquals("Bem Vindo", page.obterMensagemAlerta());
		
	}
	
	public void tearDown() {
		
	}

}
