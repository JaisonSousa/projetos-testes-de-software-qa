﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using NUnit.Framework;

namespace InoveTeste.PageObject
{

    class Contato
    {
        private RemoteWebDriver _driver;

        public Contato(RemoteWebDriver driver) => _driver = driver;

        IWebElement name => _driver.FindElementByName("your-name");
        IWebElement email => _driver.FindElementByName("your-email");
        IWebElement subject => _driver.FindElementByName("your-subject");
        IWebElement message => _driver.FindElementByName("your-message");
        IWebElement enviar => _driver.FindElementByCssSelector("input.wpcf7-form-control.wpcf7-submit");

        public void verificarExistenciaCampos()
        {
            Assert.IsTrue(name.Enabled);
            Assert.IsTrue(email.Enabled);
            Assert.IsTrue(subject.Enabled);
            Assert.IsTrue(message.Enabled);
            Assert.IsTrue(enviar.Enabled);

        }

        public void ClicarBotaoEnviar()
        {
            enviar.Click();
        }


    }
}
