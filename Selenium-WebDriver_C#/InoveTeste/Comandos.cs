﻿using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Appium.Android;


namespace InoveTeste
{
    internal class Comandos
    {
        #region Browser
       /* public static RemoteWebDriver GetBrowserLocal(RemoteWebDriver driver, String browser)
        {
            switch (browser)
            {
                case "Internet Explorer":
                    driver = new InternetExplorerDriver();
                    driver.Manage().Window.Maximize();
                    break;
                case "Chrome":
                    driver = new ChromeDriver();
                    driver.Manage().Window.Maximize();
                    break;
                default:
                    driver = new FirefoxDriver();
                    driver.Manage().Window.Maximize();
                    break;

            }

            return driver;

        }*/
        #endregion

        #region Browser Remote
        public static RemoteWebDriver GetBrowserRemote(RemoteWebDriver driver, String browser, String url)
        {
            switch (browser)
            {
                case "Internet Explorer":
                    InternetExplorerOptions cap_ie = new InternetExplorerOptions();
                    driver = new RemoteWebDriver(new Uri(url),cap_ie);
                    driver.Manage().Window.Maximize();
                    break;
                case "Chrome":
                    ChromeOptions cap_chrome = new ChromeOptions();
                    driver = new RemoteWebDriver(new Uri(url), cap_chrome);
                    driver.Manage().Window.Maximize();
                    break;
                default:
                    FirefoxOptions cap_firefox = new FirefoxOptions();
                    driver = new RemoteWebDriver(new Uri(url), cap_firefox);
                    driver.Manage().Window.Maximize();
                    break;

            }

            return driver;

        }
        #endregion

        #region Browser Remote
        public static RemoteWebDriver GetBrowserMobile(RemoteWebDriver driver, String platform,String deviceName, String BrowserName, String url)
        {
            switch (platform)
            {
                case "Android":
                    ReadOnlyDesiredCapabilities cap_android = new ReadOnlyDesiredCapabilities();
                    Desi
                    
                    break;
                case "Chrome":
                    ChromeOptions cap_chrome = new ChromeOptions();
                    driver = new RemoteWebDriver(new Uri(url), cap_chrome);
                    driver.Manage().Window.Maximize();
                    break;
                default:
                    FirefoxOptions cap_firefox = new FirefoxOptions();
                    driver = new RemoteWebDriver(new Uri(url), cap_firefox);
                    driver.Manage().Window.Maximize();
                    break;

            }

            return driver;

        }
        #endregion


        #region JavaScript
        public static void ExecuteJavaScript(IWebDriver driver, String script) 
        {
            IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
            js.ExecuteScript(script);

        }
        #endregion


    }
}
