package br.ce.wcjaison.rest.tests.refac;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.is;

import org.junit.Test;

import br.ce.wcjaison.rest.core.BaseTest;
import io.restassured.RestAssured;

public class SaldoTest extends BaseTest {
	
	
	@Test
	public void deveCalcularSaldoContas() {
		
	    Integer CONTA_ID = getIdContaPeloNome("Conta para saldo");
		
		given()
//		  .header("Authorization", "JWT " +TOKEN)
		  .when()
		  .get("/saldo")
		  .then()
		  .statusCode(200)
		  .body("find{it.conta_id == "+CONTA_ID+"}.saldo", is("534.00"))
		  .log().all()
		;
		
	}
	
	public Integer getIdContaPeloNome(String nome) {
	return	RestAssured.get("/contas?nome="+nome).then().extract().path("id[0]");
		
	}

}
