package br.ce.wcjaison.rest.tests.refac;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;

import org.junit.Test;

import br.ce.wcjaison.rest.core.BaseTest;
import br.ce.wcjaison.rest.tests.Movimentacao;
import br.ce.wcjaison.rest.utils.BarrigaUtils;
import br.ce.wcjaison.rest.utils.DataUtils;

public class MovimentacaoTest extends BaseTest {
	
	@Test
	public void deveInserirMovimentacaoComSucesso() {
		Movimentacao mov = getMovimentacaoValida();
	 given()
		  .body(mov)
		  .when()
		  .post("/transacoes")
		  .then()
		  .statusCode(201)
		  .log().all()
		;
		
	}
	
	@Test
	public void deveValidarCamposObrigatoriosMovimentacao() {

		given()
		  .body("{}")
		  .when()
		  .post("/transacoes")
		  .then()
		  .statusCode(400)
		  .body("$", hasSize(8))
		  .body("msg", hasItems(
				  "Data da Movimenta��o � obrigat�rio",
				  "Data do pagamento � obrigat�rio",
				  "Descri��o � obrigat�rio",
				  "Interessado � obrigat�rio",
				  "Valor � obrigat�rio",
				  "Valor deve ser um n�mero",
				  "Conta � obrigat�rio",
				  "Situa��o � obrigat�rio"
				  ))
		  .log().all()
		;
		
	}
	
	@Test
	public void naoDeveInserirMovimentacaoComDataFutura() {
		Movimentacao mov = getMovimentacaoValida();
		mov.setData_transacao(DataUtils.getDataDiferencaDias(2));
		given()
		  .body(mov)
		  .when()
		  .post("/transacoes")
		  .then()
		  .statusCode(400)
		  .body("$", hasSize(1))
		  .body("msg", hasItem("Data da Movimenta��o deve ser menor ou igual � data atual"))
		  .log().all()
		;
		
	}
	
	@Test
	public void naoDeveRemoverContaMovimentacao() {
		
		Integer CONTA_ID = BarrigaUtils.getIdContaPeloNome("Conta com movimentacao");
		
		given()
		  .pathParam("id", CONTA_ID)
		  .when()
		  .delete("/contas/{id}")
		  .then()
		  .statusCode(500)
		  .body("constraint", is("transacoes_conta_id_foreign"))
		  .log().all()
		;
		
	}
	
	@Test
	public void DeveRemoverMovimentacao() {
		
		Integer MOV_ID = BarrigaUtils.getIdContaPeloDescricao("Movimentacao para exclusao");
		
		given()
//		  .header("Authorization", "JWT " +TOKEN)
		  .pathParam("id", MOV_ID)
		  .when()
		  .delete("/transacoes/{id}")
		  .then()
		  .statusCode(204)
		  //.body("constraint", is("transacoes_conta_id_foreign"))
		  .log().all()
		;
		
	}	
	
	private Movimentacao getMovimentacaoValida() {
		Movimentacao mov = new Movimentacao();
		mov.setConta_id(BarrigaUtils.getIdContaPeloNome("Conta para movimentacoes"));
		mov.setDescricao("Descri��o da movimenta��o");
		mov.setEnvolvido("Envolvido na mov");
		mov.setTipo("REC");
		mov.setData_transacao(DataUtils.getDataDiferencaDias(-1));
		mov.setData_pagamento(DataUtils.getDataDiferencaDias(5));
		mov.setValor(100f);
		mov.setStatus(true);
		return mov;
		
	}

}
