package br.ce.wcjaison.rest.tests;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

import java.awt.image.DataBufferUShort;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import br.ce.wcjaison.rest.core.BaseTest;
import br.ce.wcjaison.rest.utils.DataUtils;



// Teste Git
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class BarrigaTest extends BaseTest {
	
	private String TOKEN;
	
	private static String CONTA_NAME = "Conta " + System.nanoTime();
	private static Integer CONTA_ID;
	
	@Before
	public void login() {
		Map<String, String> login = new HashMap<String, String>();
		login.put("email","jaison.sp13@gmail.com");
		login.put("senha","123456");
		
		TOKEN = given()
		    .body(login)
		.when()
		   .post("/signin")
		.then()
		   .statusCode(200)
		   .extract().path("token");	
	}
	
	@Test
	public void t01_naoDevoAcessarAPISemToken() {
		given()
		.when()
		.get("/contas")
		.then()
		.statusCode(401)
		;
		
	}
	
	@Test
	public void t02_deveIncluirContaComSucesso() {
		
	    CONTA_ID = given()
		  .header("Authorization", "JWT " +TOKEN)
		  .body("{\"nome\": \""+CONTA_NAME+"\" }")
		  .when()
		  .post("/contas")
		  .then()
		  .statusCode(201)
		  .extract().path("id")
		  
		;
		
	}
	
	@Test
	public void t03_deveAlterarContaComSucesso() {
		
		given()
		  .header("Authorization", "JWT " +TOKEN)
		  .body("{\"nome\": \""+CONTA_NAME+" alterada\" }")
		  .pathParam("id", CONTA_ID)
		  .when()
		  .put("/contas/{id}")
		  .then()
		  .log().all()
		  .statusCode(200)
		  .body("nome", is("conta alterada"))
		;
		
	}
	
	@Test
	public void t04_naoDeveInseriContaMesmoNome() {
		
		given()
		  .header("Authorization", "JWT " +TOKEN)
		  .body("{\"nome\": \\\"\"+CONTA_NAME+\" alterada\\\" }")
		  .when()
		  .post("/contas")
		  .then()
		  .log().all()
		  .statusCode(400)
		  .body("error", is("J� existe uma conta com esse nome!"))
		;
		
	}
	
	@Test
	public void t05_deveInserirMovimentacaoComSucesso() {
		Movimentacao mov = getMovimentacaoValida();
		given()
		  .header("Authorization", "JWT " +TOKEN)
		  .body(mov)
		  .when()
		  .post("/transacoes")
		  .then()
		  .statusCode(201)
		  .log().all()
		  .extract().path("id")
		;
		
	}
	
	@Test
	public void t06_deveValidarCamposObrigatoriosMovimentacao() {

		given()
		  .header("Authorization", "JWT " +TOKEN)
		  .body("{}")
		  .when()
		  .post("/transacoes")
		  .then()
		  .statusCode(400)
		  .body("$", hasSize(8))
		  .body("msg", hasItems(
				  "Data da Movimenta��o � obrigat�rio",
				  "Data do pagamento � obrigat�rio",
				  "Descri��o � obrigat�rio",
				  "Interessado � obrigat�rio",
				  "Valor � obrigat�rio",
				  "Valor deve ser um n�mero",
				  "Conta � obrigat�rio",
				  "Situa��o � obrigat�rio"
				  ))
		  .log().all()
		;
		
	}
	
	@Test
	public void t07_naoDeveInserirMovimentacaoComDataFutura() {
		Movimentacao mov = getMovimentacaoValida();
		mov.setData_transacao(DataUtils.getDataDiferencaDias(2));
		given()
		  .header("Authorization", "JWT " +TOKEN)
		  .body(mov)
		  .when()
		  .post("/transacoes")
		  .then()
		  .statusCode(400)
		  .body("$", hasSize(1))
		  .body("msg", hasItem("Data da Movimenta��o deve ser menor ou igual � data atual"))
		  .log().all()
		;
		
	}
	
	@Test
	public void t08_naoDeveRemoverContaMovimentacao() {
		given()
		  .header("Authorization", "JWT " +TOKEN)
		  .pathParam("id", CONTA_ID)
		  .when()
		  .delete("/contas/{id}")
		  .then()
		  .statusCode(500)
		  .body("constraint", is("transacoes_conta_id_foreign"))
		  .log().all()
		;
		
	}
	
	@Test
	public void t09_deveCalcularSaldoContas() {
		given()
		  .header("Authorization", "JWT " +TOKEN)
		  .when()
		  .get("/saldo")
		  .then()
		  .statusCode(200)
		  .body("find{it.conta_id == "+CONTA_ID+"}.saldo", is("100.00"))
		  .log().all()
		;
		
	}
	
	@Test
	public void t010_DeveRemoverMovimentacao() {
		given()
		  .header("Authorization", "JWT " +TOKEN)
		  .when()
		  .delete("/transacoes/1452160")
		  .then()
		  .statusCode(204)
		  //.body("constraint", is("transacoes_conta_id_foreign"))
		  .log().all()
		;
		
	}
	
	private Movimentacao getMovimentacaoValida() {
		Movimentacao mov = new Movimentacao();
		mov.setConta_id(CONTA_ID);
		mov.setDescricao("Descri��o da movimenta��o");
		mov.setEnvolvido("Envolvido na mov");
		mov.setTipo("REC");
		mov.setData_transacao(DataUtils.getDataDiferencaDias(-1));
		mov.setData_pagamento(DataUtils.getDataDiferencaDias(5));
		mov.setValor(100f);
		mov.setStatus(true);
		return mov;
		
	}

}
